var score = 0;
var keuze;

var readable = 
{
    '0': 'steen',
    '1': 'papier',
    '2': 'schaar'
};

var computer =
{
    init: function(){
        
        this.bewaren = Math.floor(Math.random() * 3);
        this.text = readable[this.bewaren];
    },
    bewaren: '', 
    text: ''
};

var order = [0, 1, 2, 0];

var winnaar = function(player, cpu)
{
    if(order[player] === order[cpu])
    {
        return 'The game is tied.';
    }
    if(order[player] === order[cpu + 1])
    {
        score++;
        return 'You won';
    }
    else
    {
        score--;
        return ' You lost ';
    }
}
    var p = document.querySelector('p');
    var assignClick = function(tag, pos){
        tag.addEventListener('click', function(){
            keuze = pos;
            computer.init();
            p.innerText = '\n' + 'the computer chose: ' + computer.text;
            p.innerText += chooseWinner(keuze, computer.bewaren);
            p.innerText += '\n' + 'Score: ' + score;
        })
    }



var images = 
{ 
    tags: document.getElementsByTagName('img'),
    init: function(){
        for(var step = 0; step < this.tags.length; step++){
            assignClick(this.tags[step], step);
        }
    }
}

images.init();
//console.log('computer:', computer);

